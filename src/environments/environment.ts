// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDMSTp9LP1Lb41w-Cffkbk2UgNj6n5Z7-M",
    authDomain: "camle-d573c.firebaseapp.com",
    databaseURL: "https://camle-d573c.firebaseio.com",
    storageBucket: "camle-d573c.appspot.com",
    messagingSenderId: "584806312822"
  }
};
