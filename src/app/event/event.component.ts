import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {
  selectedEvent: any;
  constructor(public data: DataService, private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      data.getEvent(params.id);
    });


    // this.selectedEvent = data.sampleEvent;

    // console.log(this.selectedEvent);
  }

  ngOnInit() {
  }

}
