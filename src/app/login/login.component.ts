import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public userSignInForm: FormGroup; // our model driven form
  public submitted: boolean; // keep track on whether form is submitted

  constructor(private _fb: FormBuilder, public data: DataService, private router: Router) {

  }

  ngDoCheck() {
    // console.log("Ng Do Check in Sign In Component");

    if (this.data.isLoggedIn) {
      this.router.navigate([""]);
    }
  }

  ngOnInit() {
    this.userSignInForm = this._fb.group({
      email: ['', [<any>Validators.required]],
      password: ['', [<any>Validators.required]]
    });


  }

  login(model: any, isValid: boolean) {
    this.submitted = true; // set form submit to true
    console.log(model);
    if (isValid) {

      this.data.loginUser(model).then((data: any) => {
        if(data.status)
        {
          console.log("Success " + data.message);
          // this.data.isLoggedIn = true;
          this.router.navigate([""]);
        }
        else{
          alert(data.message);
        }
      }).catch((error: any) => {
        console.log("Error " + error.message);
      });
    }
  }

}
