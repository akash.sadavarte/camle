export interface UserProfile {
      firstName: string; // required with minimum 5 chracters
      lastName: string; // required with minimum 5 chracters
      gender: String;
      city: String;
      state: String;
      country: String;
      profession: String;
      mobile: Number;
}




// //Constructor


// import { Component, ChangeDetectorRef } from '@angular/core';
// import { NavController, ViewController, LoadingController } from 'ionic-angular';
// import { Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';
// import { DomSanitizer } from '@angular/platform-browser';
// import { ReversePipe } from '../../../pipes/reverse.pipe';
// import { FileChooser } from '@ionic-native/file-chooser';
// import { File } from '@ionic-native/file';
// import { FilePath } from '@ionic-native/file-path';
// import { Data } from '../../../providers/data';
// import { Base64 } from '@ionic-native/base64';
// import { AndroidPermissions } from '@ionic-native/android-permissions';
// var Date2 = <any>Date;
// /*
//   Generated class for the Createevent page.

//   See http://ionicframework.com/docs/v2/components/#navigation for more info on
//   Ionic pages and navigation.
// */
// @Component({
//       selector: 'page-createevent',
//       templateUrl: 'createevent.html'
// })
// export class CreateeventPage {

//       formSlideOptions = {
//             resistance: true,
//             resistanceRatio: 0,
//             freeMode: true,
//       };

//       public toExpand: boolean = false;
//       public catid: number = 0;

//       public tickprogforid: number = 0;

//       public newEvent: FormGroup;

//       public events: any = {
//             eventid: '',
//             name: '',
//             eventCategory: 'Contest',
//             fromDate: '',
//             type: 'Free',
//             fromTime: '',
//             toDate: '',
//             toTime: '',
//             imgCover: '',
//             imgDisplay: '',
//             description: '',
//             awardsCertificate: '',
//             organizerFullName: '',
//             organizerEmail: '',
//             organizerPhone: '',
//             organizerWebsite: '',
//             address: '',
//             pinCode: '',
//             city: '',
//             country: '',
//             acceptanceOfCommission: false,
//             forwardWebsite: '',
//             user: '',
//             programmes: []
//       };

//       public eventProgrammes: any = [];

//       public participants: boolean;

//       fromDate: any;
//       public date: any;
//       uploadFile1: any;
//       uploadFile2: any;
//       eventCategoryModel: any;
//       // eventFromDate:any;
//       public programmeCategory: Array<string> = [];
//       public ticketSeats: Array<string> = [];
//       public ticketCategoryUser: Array<string> = [];
//       public ticketCategoryParticipant: Array<string> = [];
//       public ticketFreeBoth: Array<string> = [];
//       public ticketFreeAudience: Array<string> = [];
//       public ticketFreeParticipant: Array<string> = [];
//       public cities: Array<string> = [];
//       public country: Array<string> = [];
//       public validationErros: any;

//       public tickCatSelection = [];

//       public ticketFeeStruc = [{
//             id: 0,
//             name: 'Audience',
//             subcats: [{ name: 'Free' }, { name: 'Paid' }]
//       },
//       {
//             id: 1,
//             name: 'Participant',
//             subcats: [{ name: 'Participant (Free)' }, { name: 'Participant (Paid)' }]
//       },
//       {
//             id: 2,
//             name: 'Both',
//             subcats: [{ name: 'Free' }, { name: 'Paid' },
//             { name: 'Participant (Free)' }, { name: 'Participant (Paid)' }]
//       }];

//       public progCategories = [{
//             id: 0,
//             name: 'Contest',
//             subcats: [{ name: 'Photography' }, { name: 'Scripting/Writing' }, { name: 'Music Video' },
//             { name: 'Animations' }, { name: 'Documentary' }, { name: 'Video/Films' }]
//       },
//       {
//             id: 1,
//             name: 'Workshop',
//             subcats: [{ name: 'Training' }, { name: 'Class' }, { name: 'Courses' }]
//       },
//       {
//             id: 2,
//             name: 'College Fest',
//             subcats: [{ name: 'Short Film' }, { name: 'Photography' }, { name: 'Video/Films' },
//             { name: 'Animations' }, { name: 'Documentary' }, { name: 'PSA' }]
//       },
//       {
//             id: 3,
//             name: 'Films',
//             subcats: [{ name: 'International Films' }, { name: 'Indian' }, { name: 'Student' },
//             { name: 'Online' }, { name: 'Documentary' }, { name: 'Feature' }]
//       },
//       {
//             id: 4,
//             name: 'Birding',
//             subcats: [{ name: 'Birding' }]
//       },
//       {
//             id: 5,
//             name: 'Tour',
//             subcats: [{ name: 'Photo Walk' }, { name: 'Photo Tours' }]
//       },
//       {
//             id: 6,
//             name: 'Art/Theatre',
//             subcats: [{ name: 'Live Shows' }, { name: 'Dramas' }, { name: 'Plays' }]
//       }];

//       public image1Src: any = '';
//       public image2Src: any = '';

//       constructor(public navCtrl: NavController, private base64: Base64, private androidPermissions: AndroidPermissions, private file: File, private filePath: FilePath, private fileChooser: FileChooser, public loadingCtrl: LoadingController, public d: Data, public dom: DomSanitizer, public ref: ChangeDetectorRef, public viewCtrl: ViewController, private formBuilder: FormBuilder) {


//       }

//       ngOnInit() {

//       }

//       ionViewLoaded() {



//             this.validationErros = {
//                   'name': 'Event Name is required (Minimum 5 Characters)',
//                   'date': 'Date is Required',
//                   'number': 'Please enter a valid input',
//                   'required': 'Required',
//                   'image': 'Please upload a valid image',
//                   'organizerDetails': 'Invalid Organizer Details',
//                   'email': 'Invalid Email Address',
//                   'programme': 'Enter valid programme details',
//                   'ticket': 'Enter a valid ticket'
//             }


//             // Require the thing
//             this.ticketFreeBoth = ['Free', 'Paid', 'Participant (Free)', 'Participant (Paid)'];
//             this.ticketFreeAudience = ['Free', 'Paid'];
//             this.ticketFreeParticipant = ['Participant (Free)', 'Participant (Paid)'];
//             this.ticketCategoryUser = [
//                   'Student', 'Corporate', 'Early Bird', 'Regular', 'Late', 'Gold', 'Silver', 'Platinum'
//             ];
//             this.ticketCategoryParticipant = [
//                   'Student', 'Corporate', 'Early Bird', 'Regular', 'Late'
//             ];

//       }

//       ionViewDidLoad() {
//             console.log('Hello CreateeventPage Page');
//       }

//       addProgToForm() {
//             // +(this.events.programmes.length+1)
//             this.events.programmes.push({
//                   timestamp: Math.round(new Date().getTime() / 1000),
//                   programmeName: 'Programme name',
//                   type: '',
//                   programmeCategory: '',
//                   tickets: [],
//                   fromDate: '',
//                   fromTime: '00:00',
//                   toDate: '',
//                   toTime: '00:00',
//                   description: '',
//                   toExpand: false
//             });
//       }





//       addTicket(programme: any) {
//             console.log(programme);
//             this.events.programmes[programme].tickets.push({
//                   timestamp: Math.round(new Date().getTime() / 1000),
//                   free: 'Free',
//                   category: '',
//                   seats: '0',
//                   price: '0',
//                   date: '',
//                   toExpand: false
//             });
//       }


//       addParticipant() {
//             this.events.participantTickets.push({
//                   timestamp: Math.round(new Date().getTime() / 1000),
//                   free: 'Free',
//                   category: '',
//                   seats: '0',
//                   price: '0',
//                   date: ''
//             });
//       }

//       checkPermissons(imageNo) {

//             this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(success => {
//                   this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE]).then(() => {
//                         this.changeImage(imageNo)
//                   });
//             }).catch(() => {
//                   this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE]).then(() => {
//                         this.changeImage(imageNo)
//                   });
//             });
//       }

//       changeImage(imageNo) {
//             this.fileChooser.open()
//                   .then(uri => {
//                         //alert('URI '+uri);

//                         this.filePath.resolveNativePath(uri)
//                               .then(filePath => {
//                                     if (imageNo == 1) {
//                                           // this.image1Src=filePath;
//                                           console.log(this.image1Src);
//                                           this.base64.encodeFile(filePath).then((base64) => {
//                                                 this.image1Src = base64;
//                                           });
//                                     } else if (imageNo == 2) {
//                                           // this.image2Src=filePath;
//                                           console.log(this.image2Src);
//                                           this.base64.encodeFile(filePath).then((base64) => {
//                                                 this.image2Src = base64;
//                                           });
//                                     }
//                               })
//                               .catch(err => console.log(err));

//                   }).catch(e => console.log(e));
//       }



//       dismiss() {
//             this.viewCtrl.dismiss();
//       }



//       submitEvent() {
//             // console.log(JSON.stringify(this.events,null, 8));
//             let loader = this.loadingCtrl.create({
//                   content: "Adding event..."
//             });
//             loader.present();

//             this.d.createEvent(this.events, this.image2Src, this.image1Src).then((data: any) => {
//                   // console.log('success!!');

//                   if (typeof data == 'string') {
//                         console.log(data);
//                         loader.dismiss();
//                         this.d.presentToast(data, 5000, 'bottom', true);
//                   } else {
//                         console.log(JSON.stringify(data));
//                         loader.dismiss();
//                         this.resetAll();
//                         this.d.presentToast('Event added successfully :)', 5000, 'bottom', false);
//                   }

//             }).catch((err) => {
//                   loader.dismiss();
//                   this.d.presentToast(err, 5000, 'bottom', true);
//             })
//       }

//       toDisable() {
//             return (this.events.name.length < 5) || (this.image2Src.length == 0) || (this.events.description.length == 0) || (this.events.organizerFullName.length < 5) ||
//                   (this.events.organizerPhone.length != 10) || (this.events.organizerEmail.length == 0)
//       }

//       changeDate(value, type, toorfrom, i, j) {

//             var date = value.day.text + '/' + value.month.text + '/' + value.year.text;
//             var time;
//             if (type == 'event' || type == 'prog') {
//                   time = value.hour.value + ':' + value.minute.text;
//             }

//             if (type == 'event' && toorfrom == 'start') {
//                   console.log(date + ' - ' + time);
//                   this.events.fromDate = date;
//                   this.events.fromTime = time;
//             } else if (type == 'event' && toorfrom == 'end') {
//                   console.log(date + ' - ' + time);
//                   this.events.toDate = date;
//                   this.events.toTime = time;
//             } else if (type == 'prog' && toorfrom == 'start') {
//                   console.log(date + ' - ' + time);
//                   this.events.programmes[i].fromDate = date;
//                   this.events.programmes[i].fromTime = time;
//             } else if (type == 'prog' && toorfrom == 'end') {
//                   console.log(date + ' - ' + time);
//                   this.events.programmes[i].toDate = date;
//                   this.events.programmes[i].toTime = time;
//             } else if (type == 'ticket') {
//                   this.events.programmes[i].tickets[j].date = date;
//             }
//       }

//       changeCat(i: any) {
//             console.log(i);
//             for (var ind = 0; ind < this.progCategories.length; ind++) {
//                   console.log(this.progCategories[ind].name);
//                   if (this.progCategories[ind].name == i) {
//                         this.catid = this.progCategories[ind].id;
//                         // this.buyLeadForm.controls['SubCategory'].setValue(this.progCategories[ind].subcats[0].name);
//                         break;
//                   }
//             }
//       }

//       changeTickProgFor(i: any, index) {
//             console.log(i);
//             this.events.programmes[index].type = i;
//             for (var ind = 0; ind < this.ticketFeeStruc.length; ind++) {
//                   console.log(this.ticketFeeStruc[ind].name);
//                   if (this.ticketFeeStruc[ind].name == i) {
//                         this.tickprogforid = this.ticketFeeStruc[ind].id;
//                         // this.buyLeadForm.controls['SubCategory'].setValue(this.ticketFeeStruc[ind].subcats[0].name);
//                         break;
//                   }
//             }
//       }

//       changeProgDetail(val, field, index) {

//             console.log(val);
//             this.events.programmes[index][field] = val + '';
//             this.ref.detectChanges();
//       }

//       changeProgDetailText(val, field, index) {

//             console.log(JSON.stringify(val) + '--' + field + '--' + index);
//             this.events.programmes[index][field] = val + '';
//             this.ref.detectChanges();
//       }

//       changeTickDetail(val, field, pi, ti) {
//             this.events.programmes[pi].tickets[ti][field] = val + '';
//             this.ref.detectChanges();
//       }

//       changeFeeType(i: any, ind, j) {

//             console.log(i);
//             this.events.programmes[ind].tickets[j].free = i;
//             if (i == 'Free' || i == 'Paid') {
//                   this.tickCatSelection = [
//                         'Student', 'Corporate', 'Early Bird', 'Regular', 'Late', 'Gold', 'Silver', 'Platinum'
//                   ];
//                   console.log(this.tickCatSelection);
//             } else {
//                   this.tickCatSelection = [
//                         'Student', 'Corporate', 'Early Bird', 'Regular', 'Late'
//                   ];
//                   console.log(this.tickCatSelection);
//             }
//       }

//       changeFeeCat(val, i, j) {

//             this.events.programmes[i].tickets[j].category = val;
//       }

//       resetAll() {

//             this.image1Src = '';
//             this.image2Src = '';
//             this.events = {
//                   eventid: '',
//                   name: '',
//                   eventCategory: 'Contest',
//                   fromDate: '',
//                   type: 'Free',
//                   fromTime: '',
//                   toDate: '',
//                   toTime: '',
//                   imgCover: '',
//                   imgDisplay: '',
//                   description: '',
//                   awardsCertificate: '',
//                   organizerFullName: '',
//                   organizerEmail: '',
//                   organizerPhone: '',
//                   organizerWebsite: '',
//                   address: '',
//                   pinCode: '',
//                   city: '',
//                   country: '',
//                   acceptanceOfCommission: false,
//                   forwardWebsite: '',
//                   user: '',
//                   programmes: []
//             };
//       }

// }
