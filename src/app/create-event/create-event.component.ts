import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

//Datepicker Config
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { CustomValidator } from '../validators/custom-validator';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {
  public createEventForm: FormGroup; // our model driven form
  
  public submitted: boolean; // keep track on whether form is submitted
  public imageDisplayUrl: any;
  public imageCoverUrl: any;
  public imageCover: any;
  public imageDisplay: any;

  public editorValue: string = '';

  customValidator : CustomValidator;


  constructor(private _fb: FormBuilder, public data: DataService, private router: Router, public dpConfig: BsDatepickerConfig) {
    this.imageCoverUrl = "https://lorempixel.com/1920/600";
    this.imageDisplayUrl = "https://lorempixel.com/1920/600";

    dpConfig.dateInputFormat = 'DD/MM/YYYY';
    dpConfig.showWeekNumbers = false;
    this.customValidator = new CustomValidator();
  }

  ngOnInit() {
    this.createEventForm = this._fb.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      eventCategory: ['', [<any>Validators.required]],
      fromDate: ['', [<any>Validators.required]],
      fromTime: ['', [<any>Validators.required]],
      toDate: ['', [<any>Validators.required]],
      toTime: ['', [<any>Validators.required]],
      address: ['', [<any>Validators.required]],
      city: ['', [<any>Validators.required]],
      country: ['', [<any>Validators.required]],
      description: ['', [<any>Validators.required, Validators.minLength(40)]],
      forwardWebsite: ['', [<any>Validators.required ,this.customValidator.validateWebsite]],
      imgCover: ['', [<any>Validators.required]],
      imgDisplay: ['', [<any>Validators.required]],
      organizerFullName: ['', [<any>Validators.required]],
      organizerEmail: ['', [<any>Validators.required]],
      organizerPhone: ['', [<any>Validators.required, this.customValidator.validateMobile, Validators.minLength(10),Validators.maxLength(10)]],
      organizerWebsite: ['', [<any>Validators.required,this.customValidator.validateWebsite]],
      cost: [''],
      awardsCertificate: [''],
      acceptanceOfCommission: ['False', [<any>Validators.required]],
      user: ['', [<any>Validators.required]],
      eventid: ['', [<any>Validators.required]],
      type: [''],
      programmes: this._fb.array(
        [this.createProgramme()]
      )
    });

    this.FillForm();
  }

  createProgramme(): FormGroup {
    return this._fb.group({
      programmeName: ['', [<any>Validators.required]],
      programmeCategory: ['', [<any>Validators.required]],
      type: ['', [<any>Validators.required]],
      fromDate: ['', [<any>Validators.required]],
      fromTime: ['', [<any>Validators.required]],
      toDate: ['', [<any>Validators.required]],
      toTime: ['', [<any>Validators.required]],
      description: ['', [<any>Validators.required, Validators.minLength(40)]],
      timestamp: ['', [<any>Validators.required]],
      tickets: this._fb.array([this.createTicket()])
    });

  }

  createTicket(): FormGroup {
    return this._fb.group({
      category: ['', [<any>Validators.required]],
      date: ['', [<any>Validators.required]],
      free: ['', [<any>Validators.required]],
      price: ['', [<any>Validators.required]],
      seats: ['', [<any>Validators.required]],
      timestamp: ['', [<any>Validators.required]]
    });
  }

  addProgramme() {
    const programmeControl = <FormArray>this.createEventForm.controls['programmes'];
    programmeControl.push(this.createProgramme());
  }

  deleteProgramme(programmeIndex: number) {
    const programmeControl = <FormArray>this.createEventForm.controls['programmes'];
    if (programmeControl.controls.length == 1) {
      alert("Minimum 1 Programme Required per Event");
      return;
    }
    programmeControl.removeAt(programmeIndex);
  }

  addTicket(programmeIndex: number) {
    const programmeControl = <FormArray>this.createEventForm.controls['programmes'];
    const ticketControl = <FormArray>programmeControl.controls[programmeIndex].get("tickets");
    ticketControl.push(this.createTicket());
  }

  deleteTicket(programmeIndex: number, ticketIndex: number) {
    const programmeControl = <FormArray>this.createEventForm.controls['programmes'];
    const ticketControl = <FormArray>programmeControl.controls[programmeIndex].get("tickets");
    if (ticketControl.controls.length == 1) {
      alert("Minimum 1 Ticket Required per Programme");
      return;
    }
    ticketControl.removeAt(ticketIndex);
  }

  FillForm(): any {
    this.createEventForm.get("eventCategory").setValue(this.data.eventCategories[0].name);
    this.createEventForm.get("city").setValue("Mumbai");
    this.createEventForm.get("country").setValue("India");
    // this.createEventForm.patchValue(this.data.selectedEvent);
    this.getCurrentEventSubCategories();
  }

  public detectFile(event: any, type: string) {
    console.log("CreateEventComponent : Detect File Method Called");

    if (type == "Cover") {
      this.imageCover = event.target.files;
      console.log(this.imageCover);
    }
    else {
      this.imageDisplay = event.target.files;
      console.log(this.imageDisplay);
    }
  }

  imageUpload(type: string): any {
    console.log("CreateEventComponent : Image Upload Function Called and Type is " + type);
    let file: any;
    if (type == "Cover") {
      file = this.imageCover.item(0);
      console.log("Cover");
      console.log(this.imageCover);
    }
    else {
      file = this.imageDisplay.item(0);
      console.log("Display");
      console.log(this.imageDisplay);
    }
    this.data.imageUpload(file, type).then((data: any) => {
      if (type == "Cover") {
        console.log("Cover after uploading");
        this.imageCoverUrl = data;
        this.createEventForm.patchValue({
          imgCover: data
        });
      }
      else {
        console.log("Display after uploading");
        this.imageDisplayUrl = data;
        this.createEventForm.patchValue({
          imgDisplay: data
        });
      }
    });
  }

  getCurrentEventSubCategories(): any {
    for (let i = 0; i < this.data.eventCategories.length; i++) {
      if (this.data.eventCategories[i].name == this.createEventForm.value.eventCategory) {
        return this.data.eventCategories[i].subcats;
      }
    }
  }

  getProgrammes() {
    return <FormArray>this.createEventForm.get('programmes');
  }

  getTickets(programmeIndex: number) {
    const programmeControl = <FormArray>this.createEventForm.controls['programmes'];
    return <FormArray>programmeControl.controls[programmeIndex].get("tickets");
  }

  public createEvent() {
    this.submitted = true;
    console.log(this.createEventForm.value);
    if (this.createEventForm.valid) {
      this.data.createEvent(this.createEventForm.value).then((data: any) => {
        if (data.status) {
          alert(data.message);
          this.router.navigate(['/my-events']);
        } else {
          this.submitted = false;
          alert(data.message);
        }
      });
    }
    else {
      this.data.createEvent(this.createEventForm.value);
      console.log("Form Incomplete");

    }

  }

}
