import { AbstractControl } from "@angular/forms";

export class CustomValidator{

    constructor(){

    }

    confirmPassword(c: AbstractControl): { invalid: boolean } {
        if (c.get('password').value !== c.get('confirm_password').value) {
            return {invalid: true};
        }
    }

    validateMobile(){

    }

    validateLandline(){

    }

    validateEmail(control: AbstractControl){
        return control.value.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) ? null : {
            validateEmail: {
              valid: false
            }
          };
    }
    

    validateWebsite(control: AbstractControl){
        return control.value.match("^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$") ? null : {
            validateWebsite: {
              valid: false
            }
          };
    }
}
