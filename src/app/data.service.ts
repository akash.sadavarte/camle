import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

//AngularFire
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import { log } from 'util';
import { Router } from '@angular/router';
import { SeoService } from './seo.service';


@Injectable()
export class DataService {

  //Ng Models
  isLoggedIn: Boolean = false;
  currentCity: String = "Cities";
  searchText: String = "";
  currentUser: any = null;
  selectedCategoryId = 0;
  selectedDateCategory = "All";
  selectedEventType = "Both";



  //Event
  eventCategories: any;


  //Filters
  dateCategories: any;



  //Variables
  cities: Observable<any[]>;

  //Places
  countries: any;

  //SampleEvent
  sampleEvent: any;

  //Redirect Check Variable
  pageRedirectURL: string = null;
  pageRedirectEvent: any = null;
  pageRedirectTickets: any = null;

  // Event Objects
  allEvents: Observable<any[]>;
  myEvents: Observable<any[]>;
  myBookings: Observable<any[]>;
  favouriteEvents: Observable<any[]>;
  favouriteEventsForList: Observable<any[]>;
  userProfile: Observable<any>;
  userProfileInfo: any;
  selectedEvent: any;
  eventQueryStartsWithId: string = "";


  bookedTicketsArray: any[] = [];

  constructor(private seo: SeoService, private afDatabase: AngularFireDatabase, private afAuth: AngularFireAuth, private router: Router) {
    this.getAllCities();
    this.getAllEvents();
    this.eventCategories = eventCategories;
    this.dateCategories = dateCategories;

    this.countries = countries;
    this.sampleEvent = sampleEvent;
    this.afAuth.authState.subscribe((user: firebase.User) => {
      if (user) {
        this.isLoggedIn = true;
        this.currentUser = user;
        this.loginRedirect();
        this.getMyEvents();
        this.getFavouriteEvents();
        this.getMyBookings();
        this.getUserProfile();
      }
      else {
        this.isLoggedIn = false;
        this.currentUser = null;
      }
    });
  }

  //Change Category Id
  changeCategoryId(index: any): any {
    this.selectedCategoryId = index;
    console.log("Event Category changed to " + this.selectedCategoryId);
  }


  //Cities
  getAllCities(): any {
    this.cities = this.afDatabase.list('cities').valueChanges();
  }

  getFavouriteEvents(): any {
    this.favouriteEventsForList = this.afDatabase.list("/favourite-events/" + this.currentUser.uid).valueChanges();
    this.afDatabase.object("/favourite-events/" + this.currentUser.uid).valueChanges().subscribe((data: any) => {
      // console.log("User Profile Info in Edit Profile Page");
      // console.log(data);  
      this.favouriteEvents = data;
    });;
  }

  getMyBookings(): any {
    this.myBookings = this.afDatabase.list("users/" + this.currentUser.uid + "/bookings").valueChanges();
  }

  getUserProfile(): any {
    this.afDatabase.object("users/" + this.currentUser.uid + "/profile").valueChanges().subscribe((data: any) => {
      // console.log("User Profile Info in Edit Profile Page");
      // console.log(data);  
      this.userProfileInfo = data;
    });
  }


  setCurrentCity(city: any) {
    // console.log("Current city set to " + city);
    this.currentCity = city;
  }

  //My Events
  getMyEvents() {
    console.log("My Events Called after logging in  " + 'my-events/' + this.currentUser.uid);
    this.myEvents = this.afDatabase.list("/my-events/" + this.currentUser.uid + "/").valueChanges();
  }

  //User

  createUser(model: any) {
    return this.afAuth.auth.createUserWithEmailAndPassword(model.email, model.password).then((user: any) => {
      // console.log("User Created successfully");
      // console.log(user);
      model.password = null;
      model.confirmPassword = null;
      return this.afDatabase.object("users/" + user.uid + "/profile").set(model).then((success: any) => {
        return { status: true, message: "User Created Successfully" };
      })

    }).catch((error: any) => {
      console.log("Couldn't create user. Please try again.");
      return { status: false, message: error.message };
    });

  }

  editUser(model: any) {
    console.log("Edit User : Data Service Called");

    if (this.currentUser) {
      model.password = null;
      model.confirmPassword = null;
      return this.afDatabase.object("users/" + this.currentUser.uid + "/profile").set(model).then((success: any) => {
        return { status: true, message: "User Modified Successfully" };
      }).catch((error: any) => {
        console.log("Couldn't create user. Please try again.");
        return { status: false, message: error.message };
      });
    }
  }

  userSignOut() {
    this.afAuth.auth.signOut().then((response: any) => {
      console.log("User Logged Out Successfully");
    }).catch((error: any) => {
      console.log("User couldn't be Logged Out")
    });
  }

  loginUser(model: any) {
    return this.afAuth.auth.signInWithEmailAndPassword(model.email, model.password).then((success: any) => {
      return { status: true, message: "User Logged In Successfully" }
    }).catch((error: any) => {
      console.log("Couldn't Log User In");
      console.log(error);
      return { status: false, message: error.message }
    });
  }
  loginUserWithGmail() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then((data: any) => {
      console.log("Google OAuth Successful");
    }, (error: any) => {
      console.log(error);
      console.log("Some error occured while logging from Google");
    });
  }
  loginUserWithFacebook() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider()).then((data: any) => {
      console.log("Facebook OAuth Successful");
    }, (error: any) => {
      console.log(error);
      console.log("Some error occured while logging from Facebook");
    });
  }

  loginUserWithTwitter() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.TwitterAuthProvider()).then((data: any) => {
      console.log("Twitter OAuth Successful");
    }, (error: any) => {
      console.log(error);
      console.log("Some error occured while logging from Twitter");
    });
  }


  //Image Upload

  imageUpload(file: File, type: string): any {
    console.log("DataService : Image Upload Function Called");
    let storageRef = firebase.storage().ref();
    return storageRef.child("events").child("akash").child(type).put(file).then((data: any) => {
      console.log("File Uploaded successfully");
      console.log(data);
      return data.downloadURL;
    }).catch((error: any) => {
      console.log("Unable to store file");
      console.log(error);
      return null;
    });

  }


  //Event

  //Get Events

  getAllEvents() {
    this.allEvents = this.afDatabase.list('events').valueChanges();
  }

  getEvent(id: any) {
    // console.log("Fetching Event with Id " + id);
    // console.log("Selected Event is ");
    // console.log(this.selectedEvent);
    if (this.selectedEvent) {
      if (this.selectedEvent.eventid == id) {
        this.seo.generateTags({
          title: this.selectedEvent.name,
          description: this.selectedEvent.description,
          image: this.selectedEvent.imgDisplay,
          slug: 'event-page'
        });
        return this.selectedEvent;
      }
    }
    else {
      console.log("No Selected Event Else");
      firebase.database().ref('/events/' + id).once('value').then((snapshot) => {
        console.log(snapshot.val());
        this.selectedEvent = snapshot.val();

        this.seo.generateTags({
          title: this.selectedEvent.name,
          description: this.selectedEvent.description,
          image: this.selectedEvent.imgDisplay,
          slug: 'event-page'
        });
      });
    }
  }

  //CreateEvent

  private setEventCost(event: any) {
    let cost = 0;
    for (let i = 0; i < event.programmes.length; i++) {
      let programme = event.programmes[i];
      for (let j = 0; j < programme.tickets.length; j++) {
        let ticket = programme.tickets[j];
        if (cost > parseInt(ticket.price)) {
          cost = ticket.price;
        }
      }
    }
    event.cost = cost;
    if (cost == 0) {
      event.type = "Free";
    }
    else {
      event.type = "Paid";
    }
    return event;
  }

  public createEvent(event: any): any {
    if (!this.currentUser) {
      // console.log("No user logged in");

      this.pageRedirectURL = this.router.url;
      this.pageRedirectEvent = event;
      this.pageRedirectURL = "create-event";
      this.pageRedirectTickets = null;
      this.router.navigate(["/login"]);
    }
    else if (this.pageRedirectEvent && this.pageRedirectURL == "create-event") {
      console.log("Page redirection Event Save");
      this.createEventFunction(event).then((data: any) => {
        return data;
      }, (error: any) => {
        return error;
      });
      this.pageRedirectEvent = null;
      this.pageRedirectURL = null;
    }
    else if (!this.pageRedirectEvent) {
      this.createEventFunction(event).then((data: any) => {
        return data;
      }, (error: any) => {
        return error;
      });
    }

  }


  createEventFunction(event: any): any {
    event = this.setEventCost(event);
    event.user = this.currentUser.uid;
    return this.afDatabase.list("/events").push(event).then((data: any) => {
      let eventid = data.key;
      console.log("Events Entered in Events");
      event.eventid = eventid;
      this.afDatabase.object("/events/" + eventid + "/eventid").set(eventid);
      return this.afDatabase.object("/my-events/" + this.currentUser.uid + "/" + eventid).set(event).then((data: any) => {
        console.log("Events Entered in My Events");
        return { status: true, message: "Event Created Successfully" };
      },
        (error: any) => {
          return { status: true, message: "My Events Entry Error" };
        });
    },
      (error: any) => {
        return { status: true, message: "Events Entry Error" };
      });
  }

  editEvent(event: any) {

    let eventId = event.eventid;
    event = this.setEventCost(event);
    event.user = this.currentUser.uid;

    return this.afDatabase.object("/events/" + eventId).set(event).then((data: any) => {
      // let eventid = data.key;
      console.log("Events Editted in Events");
      this.afDatabase.object("/events/" + eventId + "/eventId").set(eventId);
      return this.afDatabase.object("/my-events/" + this.currentUser.uid + "/" + eventId).set(event).then((data: any) => {
        console.log("Events Editted in My Events");
        return { status: true, message: "Event Edited Successfully" };
      },
        (error: any) => {
          return { status: true, message: "Edit Event : My Events Entry Error" };
        });
    },
      (error: any) => {
        return { status: true, message: "Edit Event : Events Entry Error" };
      });
  }

  //Edit Event

  public bookTickets(event: any, tickets: any): any {
    console.log(event);
    console.log(tickets);

    let booking = {
      event: event,
      tickets: tickets
    }

    if (!this.currentUser) {
      this.pageRedirectURL = this.router.url;
      this.pageRedirectEvent = event;
      this.pageRedirectTickets = booking;
      this.router.navigate(["/login"]);
    } else {
      this.bookTicketsFunction(booking).then((data: any) => {
        return data;
      }, (error: any) => {
        return error;
      });
    }
  }


  private bookTicketsFunction(booking: any): any {
    // console.log("Book Tickets Private Function Called");
    // console.log("users/" + this.currentUser.uid + "/bookings");
    // console.log(booking);

    booking.user = {
      "id": this.currentUser.uid,
      "email": this.currentUser.email
    }

    return this.afDatabase.list("users/" + this.currentUser.uid + "/bookings").push(booking).then((data: any) => {
      let bookingId = data.key;
      booking["bookingId"] = data.key;
      return this.afDatabase.object("bookings/" + booking.event.eventid + "/" + this.currentUser.uid + "/bookings").set(booking).then((data: any) => {
        return this.afDatabase.object("bookingsById/" + bookingId).set(booking).then((data: any) => {
          return { status: true, message: "Booking Done Successfully" };
        }, (error: any) => {
          return { status: true, message: "Bookings by ID Entered Incorrectly" };
        });
      },
        (error: any) => {
          return { status: false, message: "Booking Unsuccessful" };
        });

    }, (error: any) => {
      return { status: false, message: "Booking Unsuccessful" };
    });
  }

  //Favourite Event


  modifyFavouriteEvents(event: any) {
    // console.log("Current Favourite Events");
    // console.log(this.favouriteEvents);

    let eventId = event.eventid;
    if (this.currentUser) {
      if (!this.favouriteEvents) {
        return this.afDatabase.object("/favourite-events/" + this.currentUser.uid + "/" + eventId).set(event).then((data: any) => {
          return { status: true, message: "Favourite Events Edited" };
        }, (error: any) => {
          return { status: false, message: "Favourite Events Edited Unsuccessfully" };
        });
      }
      else if (this.favouriteEvents[eventId]) {
        return this.afDatabase.object("/favourite-events/" + this.currentUser.uid + "/" + eventId).remove().then((data: any) => {
          return { status: true, message: "Favourite Events Edited" };
        }, (error: any) => {
          return { status: false, message: "Favourite Events Edited Unsuccessfully" };
        });
      } else {
        return this.afDatabase.object("/favourite-events/" + this.currentUser.uid + "/" + eventId).set(event).then((data: any) => {
          return { status: true, message: "Favourite Events Edited" };
        }, (error: any) => {
          return { status: false, message: "Favourite Events Edited Unsuccessfully" };
        });
      }
    }
  }


  loginRedirect() {
    if (this.pageRedirectURL) {
      if (this.pageRedirectURL.startsWith("/book")) {
        this.bookTickets(this.pageRedirectEvent, this.pageRedirectTickets).then((data: any) => {
          this.pageRedirectURL = null;
          this.pageRedirectEvent = null;
          this.pageRedirectTickets = null;
        });
      }
      if (this.pageRedirectURL.startsWith("/create-event")) {
        this.createEvent(this.pageRedirectEvent).then((data: any) => {
          this.pageRedirectURL = null;
          this.pageRedirectEvent = null;
          this.pageRedirectTickets = null;
        });
      }
    }

  }

  visitEvent(event: any) {
    console.log(event);
    this.selectedEvent = event;
    this.router.navigate(["/event/" + event.eventid]);
  }

  visitBookEvent(event: any) {
    console.log(event);
    this.selectedEvent = event;
    this.router.navigate(["/book/" + event.eventid]);

    for (let i = 0; i < this.selectedEvent.programmes.length; i++) {
      let programme = this.selectedEvent.programmes[i];
      for (let j = 0; j < programme.tickets.length; j++) {
        this.bookedTicketsArray[i] = {
          [j]: 0
        };
      }
    }
  }

  visitEditEvent(event: any) {
    // console.log(event);
    this.selectedEvent = event;
    this.router.navigate(["edit-event"]);
  }

}


//States
let states = [
  { "key": "AN", "name": "Andaman and Nicobar Islands" },
  { "key": "AP", "name": "Andhra Pradesh" },
  { "key": "AR", "name": "Arunachal Pradesh" },
  { "key": "AS", "name": "Assam" },
  { "key": "BR", "name": "Bihar" },
  { "key": "CG", "name": "Chandigarh" },
  { "key": "CH", "name": "Chhattisgarh" },
  { "key": "DH", "name": "Dadra and Nagar Haveli" },
  { "key": "DD", "name": "Daman and Diu" },
  { "key": "DL", "name": "Delhi" },
  { "key": "GA", "name": "Goa" },
  { "key": "GJ", "name": "Gujarat" },
  { "key": "HR", "name": "Haryana" },
  { "key": "HP", "name": "Himachal Pradesh" },
  { "key": "JK", "name": "Jammu and Kashmir" },
  { "key": "JH", "name": "Jharkhand" },
  { "key": "KA", "name": "Karnataka" },
  { "key": "KL", "name": "Kerala" },
  { "key": "LD", "name": "Lakshadweep" },
  { "key": "MP", "name": "Madhya Pradesh" },
  { "key": "MH", "name": "Maharashtra" },
  { "key": "MN", "name": "Manipur" },
  { "key": "ML", "name": "Meghalaya" },
  { "key": "MZ", "name": "Mizoram" },
  { "key": "NL", "name": "Nagaland" },
  { "key": "OR", "name": "Odisha" },
  { "key": "PY", "name": "Puducherry" },
  { "key": "PB", "name": "Punjab" },
  { "key": "RJ", "name": "Rajasthan" },
  { "key": "SK", "name": "Sikkim" },
  { "key": "TN", "name": "Tamil Nadu" },
  { "key": "TS", "name": "Telangana" },
  { "key": "TR", "name": "Tripura" },
  { "key": "UP", "name": "Uttar Pradesh" },
  { "key": "UK", "name": "Uttarakhand" },
  { "key": "WB", "name": "West Bengal" }
];


//Event Categories and Sub Categories

let dateCategories = ["Any", "Today", "This Week", "This Month", "This Year"]

let eventCategories: any = [
  {
    id: 0,
    name: 'Contest',
    subcats: [{ name: 'Photography' }, { name: 'Scripting/Writing' }, { name: 'Music Video' },
    { name: 'Animations' }, { name: 'Documentary' }, { name: 'Video/Films' }]
  },
  {
    id: 1,
    name: 'Workshop',
    subcats: [{ name: 'Training' }, { name: 'Class' }, { name: 'Courses' }]
  },
  {
    id: 2,
    name: 'College Fest',
    subcats: [{ name: 'Short Film' }, { name: 'Photography' }, { name: 'Video/Films' },
    { name: 'Animations' }, { name: 'Documentary' }, { name: 'PSA' }]
  },
  {
    id: 3,
    name: 'Films',
    subcats: [{ name: 'International Films' }, { name: 'Indian' }, { name: 'Student' },
    { name: 'Online' }, { name: 'Documentary' }, { name: 'Feature' }]
  },
  {
    id: 4,
    name: 'Birding',
    subcats: [{ name: 'Birding' }]
  },
  {
    id: 5,
    name: 'Tour',
    subcats: [{ name: 'Photo Walk' }, { name: 'Photo Tours' }]
  },
  {
    id: 6,
    name: 'Art/Theatre',
    subcats: [{ name: 'Live Shows' }, { name: 'Dramas' }, { name: 'Plays' }, { name: 'Scripting/Writing' }]
  }
];


let countries = [
  {
    id: 0,
    name: "India"
  }
];


let sampleEvent = {
  "acceptanceOfCommission": true,
  "address": "JN2, 13 A-7 Vashi",
  "awardsCertificate": "<p>Aw</p>\n",
  "city": "Mumbai",
  "cost": "0",
  "country": "India",
  "description": "<p>Desc</p>\n",
  "eventCategory": "Art/Theatre",
  "eventid": "-K_n9FG8r6tMse69to09",
  "forwardWebsite": "",
  "fromDate": "05/01/2017",
  "fromTime": "13:00",
  "imgCover": "https://firebasestorage.googleapis.com/v0/b/camle-d573c.appspot.com/o/events%2F-K_n9FG8r6tMse69to09%2Fcover.jpg?alt=media&token=b814be6c-5f9d-4fb0-bf05-5edd410f5c3f",
  "imgDisplay": "https://firebasestorage.googleapis.com/v0/b/camle-d573c.appspot.com/o/events%2F-K_n9FG8r6tMse69to09%2Finitial.jpg?alt=media&token=1f5b9d0e-044f-482d-9105-684c458e282b",
  "name": "Plasma Touch!!",
  "organizerEmail": "akashs25@gmail.com",
  "organizerFullName": "Akash",
  "organizerPhone": "8097200240",
  "organizerWebsite": "gogole.com",
  "pinCode": "400703",
  "programmes": [{
    "description": "Desc",
    "fromDate": "11/01/2017",
    "fromTime": "01:00",
    "programmeCategory": "Scripting/Writing",
    "programmeName": "Cat",
    "tickets": [{
      "category": "Early Bird",
      "date": "17/01/2017",
      "free": "Participant (Free)",
      "price": "0",
      "seats": "30",
      "timestamp": 1483695608539
    }, {
      "category": "Early Bird",
      "date": "17/01/2017",
      "free": "Participant (Paid)",
      "price": "1000",
      "seats": "30",
      "timestamp": 1483695754682
    }],
    "timestamp": 1483695608538,
    "toDate": "11/01/2017",
    "toTime": "02:00",
    "type": "Both"
  }, {
    "description": "Desc",
    "fromDate": "05/01/2017",
    "fromTime": "00:00",
    "programmeCategory": "Photography",
    "programmeName": "cat3",
    "tickets": [{
      "category": "Early Bird",
      "date": "04/01/2017",
      "free": "Participant (Free)",
      "price": "0",
      "seats": "30",
      "timestamp": 1483747916736
    }, {
      "category": "Corporate",
      "date": "03/01/2017",
      "free": "Participant (Paid)",
      "price": "1000",
      "seats": "30",
      "timestamp": 1483748422572
    }, {
      "category": "Corporate",
      "date": "",
      "free": "Paid",
      "price": "1000",
      "seats": "0",
      "timestamp": 1486446454414
    }],
    "timestamp": 1483747916736,
    "toDate": "11/01/2017",
    "toTime": "00:00",
    "type": "Both"
  }],
  "toDate": "05/01/2017",
  "toTime": "14:00",
  "type": "Paid",
  "user": "ZxmD47pCdXMumUIHLoWXbmNou1C2"
}


let ticketFeeStructure = [
  {
    id: 0,
    name: 'Audience',
    subcats: [{ name: 'Free' }, { name: 'Paid' }]
  },
  {
    id: 1,
    name: 'Participant',
    subcats: [{ name: 'Participant (Free)' }, { name: 'Participant (Paid)' }]
  },
  {
    id: 2,
    name: 'Both',
    subcats: [{ name: 'Free' }, { name: 'Paid' },
    { name: 'Participant (Free)' }, { name: 'Participant (Paid)' }]
  }
];
