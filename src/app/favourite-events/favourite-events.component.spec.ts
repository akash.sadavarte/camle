import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavouriteEventsComponent } from './favourite-events.component';

describe('FavouriteEventsComponent', () => {
  let component: FavouriteEventsComponent;
  let fixture: ComponentFixture<FavouriteEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavouriteEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavouriteEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
