import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-favourite-events',
  templateUrl: './favourite-events.component.html',
  styleUrls: ['./favourite-events.component.css']
})
export class FavouriteEventsComponent implements OnInit {

  constructor(public data: DataService) { }

  ngOnInit() {
  }

}
