import { Pipe, PipeTransform } from '@angular/core';
import { DataService } from './data.service';
declare var moment: any;

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {


  constructor(public data: DataService) {

  }

  transform(events: any, city?: any, searchText?: any, categoryId?: any, filterDays?: any, eventType?: any): any {
    if (!events)
      return null;

    // console.log(events);

    return events.filter((event: any) => {
      return this.checkCity(event.city, city)
        && this.checkSearchText(event.name, searchText)
        && this.checkEventCategory(event.eventCategory, categoryId)
        && this.checkDate(event.fromDate, event.toDate, filterDays)
        && this.checkEventType(event.type, eventType);
    });


  }


  checkEventType(eventType: any, filterType: any): any {
    if (!filterType || filterType == 'Both')
      return true;
    else
      return eventType.toLowerCase().includes(filterType.toLowerCase());

  }


  checkDate(eventFromDate: any, eventToDate: any, filterDays: any) {
    // return true;
    var currentDate = new Date();

    let categories = this.data.eventCategories;

    if (filterDays == "Any") {
      return true;
    }
    else if (filterDays == "Today") {
      return moment(currentDate).isBetween(eventFromDate, eventToDate);
    }
    else if (filterDays == "This Week") {
      let currentWeekNumber = moment(currentDate).week();
      let startWeekNumber = moment(eventFromDate).week();
      let endWeekNumber = moment(eventToDate).week();

      console.log("Start Week Number : " + startWeekNumber);
      console.log("Current Week Number : " + currentWeekNumber);
      console.log("End Week Number : " + endWeekNumber);


      return (startWeekNumber <= currentWeekNumber && currentWeekNumber <= endWeekNumber);
    }
    else if (filterDays == "This Month") {
      let currentMonthNumber = moment(currentDate).month();
      let startMonthNumber = moment(eventFromDate).month();
      let endMonthNumber = moment(eventToDate).month();
      console.log("Start Month Number : " + startMonthNumber);
      console.log("Current Month Number : " + currentMonthNumber);
      console.log("End Month Number : " + endMonthNumber);
      return (startMonthNumber <= currentMonthNumber && currentMonthNumber <= endMonthNumber);
    }
    else if (filterDays == "This Year") {
      let currentYearNumber = moment(currentDate).year();
      let startYearNumber = moment(eventFromDate).year();
      let endYearNumber = moment(eventToDate).year();
      console.log("Start Year Number : " + startYearNumber);
      console.log("Current Year Number : " + currentYearNumber);
      console.log("End Year Number : " + endYearNumber);
      return (startYearNumber <= currentYearNumber && currentYearNumber <= endYearNumber);
    }
    else {
      return true;
    }
  }


  checkEventCategory(eventCategory: any, categoryId) {
    if (!categoryId || !eventCategory)
      return true;
    let categories = this.data.eventCategories;
    return eventCategory.toLowerCase().includes(categories[categoryId].name.toLowerCase());
  }

  checkSearchText(eventName: any, searchText: any): any {
    if (!searchText || searchText == '')
      return true;
    else
      return eventName.toLowerCase().includes(searchText.toLowerCase());
  }

  checkCity(eventCity: any, city: any): any {
    if (!city || city == 'Cities')
      return true;
    else
      return eventCity.toLowerCase().includes(city.toLowerCase());
  }

}
