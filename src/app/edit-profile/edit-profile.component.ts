import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../data.service';
import { Router } from '@angular/router';
import { UserProfile } from '../models/User';
import { CustomValidator } from '../validators/custom-validator';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

  public userSignUpForm: FormGroup; // our model driven form
  public submitted: boolean; // keep track on whether form is submitted
  private valueFilled: boolean;
  customValidator : CustomValidator;
  constructor(private _fb: FormBuilder, private data: DataService, private router: Router) {

  }

  // this.userSignUpForm = this._fb.group({
  //   name: ['', [<any>Validators.required, <any>Validators.minLength(5)]],
  //   address: this._fb.group({
  //     street: ['', <any>Validators.required],
  //     postcode: ['']
  //   })
  // });

  ngOnInit() {
    this.userSignUpForm = this._fb.group({
      email: ['', [<any>Validators.required, this.customValidator.validateEmail]],
      // password: ['', [<any>Validators.required]],
      // confirmPassword: ['', [<any>Validators.required]],
      firstName: ['', [<any>Validators.required]],
      lastName: ['', [<any>Validators.required]],
      mobile: ['', [<any>Validators.required, Validators.minLength(10),Validators.maxLength(10), this.customValidator.validateMobile]],
      city: ['', [<any>Validators.required]],
      state: ['', [<any>Validators.required]],
      country: ['', [<any>Validators.required]],
      gender: ['Male', [<any>Validators.required]],
      profession: ['', [<any>Validators.required]]
    });

    this.valueFilled = false;

    // this.data.userProfile.subscribe((data: any) => {
    //   console.log("User Profile Info in Edit Profile Page");
    //   console.log(data);
    // });

  }

  ngDoCheck() {
    // console.log("Ng Do Check in Edit Profile");

    if (!this.valueFilled) {
      // console.log("Ng Do Check in Edit Profile : Value Filled False");

      if (this.data.userProfileInfo) {
        // console.log("Ng Do Check in Edit Profile : Data Profile Defined or Not Null");

        this.userSignUpForm.patchValue(this.data.userProfileInfo);
        this.valueFilled = true;
      }
    }
  }




  save(model: UserProfile, isValid: boolean) {
    this.submitted = true; // set form submit to true
    if (isValid) {

      this.data.editUser(model).then((success: any) => {
        {
          console.log("Success " + success.message);

          // this.data.isLoggedIn = true;
          // this.router.navigate([""]);
        }
      }).catch((error: any) => {
        console.log("Error " + error.message);

      });


    }
  }

}
