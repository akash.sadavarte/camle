import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


//AngularFire
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../environments/environment';

//Ngx-Bootstrap
// import { BsDropdownModule } from 'ngx-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { AccordionModule } from 'ngx-bootstrap/accordion';

//Ngx-Ckeditor
import { CKEditorModule } from 'ngx-ckeditor';

//NgxShareButtons
import { ShareButtonModule } from 'ngx-sharebuttons';


//Components
import { HomeComponent } from './home/home.component';
import { EventListComponent } from './event-list/event-list.component';
import { EventComponent } from './event/event.component';
import { BookingComponent } from './booking/booking.component';
import { CreateEventComponent } from './create-event/create-event.component';
import { EditEventComponent } from './edit-event/edit-event.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { MyBookingsComponent } from './my-bookings/my-bookings.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { DataService } from './data.service';
import { CommonModule } from '@angular/common';
import { FavouriteEventsComponent } from './favourite-events/favourite-events.component';
import { MyEventsComponent } from './my-events/my-events.component';
import { FilterPipe } from './filter.pipe';
import { SeoService } from './seo.service';




@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    EventListComponent,
    EventComponent,
    BookingComponent,
    CreateEventComponent,
    EditEventComponent,
    LoginComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    EditProfileComponent,
    MyBookingsComponent,
    FavouriteEventsComponent,
    MyEventsComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase), // imports firebase/app needed for everything
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireDatabaseModule,// imports firebase/database, only needed for auth features,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    AccordionModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    CKEditorModule,
    HttpClientModule,
    HttpClientJsonpModule,
    ShareButtonModule.forRoot()
  ],
  providers: [
    DataService,
    SeoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
