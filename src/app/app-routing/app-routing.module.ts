import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { LoginComponent } from '../login/login.component';
import { SignUpComponent } from '../sign-up/sign-up.component';
import { CreateEventComponent } from '../create-event/create-event.component';
import { EventListComponent } from '../event-list/event-list.component';
import { EventComponent } from '../event/event.component';
import { BookingComponent } from '../booking/booking.component';
import { MyEventsComponent } from '../my-events/my-events.component';
import { FavouriteEventsComponent } from '../favourite-events/favourite-events.component';
import { MyBookingsComponent } from '../my-bookings/my-bookings.component';
import { EditProfileComponent } from '../edit-profile/edit-profile.component';
import { EditEventComponent } from '../edit-event/edit-event.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'sign-up',
    component: SignUpComponent,
  },
  {
    path: 'create-event',
    component: CreateEventComponent,
  },
  {
    path: 'event-list',
    component: EventListComponent,
  },
  {
    path: 'event/:id',
    component: EventComponent,
  },
  {
    path: 'book/:id',
    component: BookingComponent,
  },
  {
    path: 'my-events',
    component: MyEventsComponent
  },
  {
    path: 'favourite-events',
    component: FavouriteEventsComponent
  },
  {
    path: 'my-bookings',
    component: MyBookingsComponent
  },
  {
    path: 'edit-profile',
    component: EditProfileComponent
  },
  {
    path: 'edit-event',
    component: EditEventComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }