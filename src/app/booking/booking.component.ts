import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {
  selectedEvent: any;
  constructor(public data: DataService, private route: ActivatedRoute) {

  }

  ngOnInit() {
  }

  bookedTickets: any = null;


  public bookTicket(programmeNumber: number, ticketNumber: number) {
    // console.log(programmeNumber + " Programme " + ticketNumber + " Ticket Number " + value + " Tickets");
    // console.log("Selected Event");
    // console.log(this.data.selectedEvent);

    if (this.data.selectedEvent.programmes[programmeNumber].tickets[ticketNumber].seats < this.data.bookedTicketsArray[programmeNumber][ticketNumber]) {
      this.data.bookedTicketsArray[programmeNumber][ticketNumber] = this.data.selectedEvent.programmes[programmeNumber].tickets[ticketNumber].seats;
    }

  }

  public bookTicketsForEvent() {
    // console.log(this.bookedTickets);
    this.data.bookTickets(this.data.selectedEvent, this.data.bookedTicketsArray);
  }

}
