import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserProfile } from '../models/User';
import { DataService } from '../data.service';
import { Router } from '@angular/router';
import { log } from 'util';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  public userSignUpForm: FormGroup; // our model driven form
  public submitted: boolean; // keep track on whether form is submitted

  constructor(private _fb: FormBuilder, private data: DataService, private router: Router) {

  }

  ngDoCheck() {
    console.log("Ng Do Check in Sign Up Component");

    if (this.data.isLoggedIn) {
      this.router.navigate([""]);
    }
  }

  // this.userSignUpForm = this._fb.group({
  //   name: ['', [<any>Validators.required, <any>Validators.minLength(5)]],
  //   address: this._fb.group({
  //     street: ['', <any>Validators.required],
  //     postcode: ['']
  //   })
  // });

  ngOnInit() {
    this.userSignUpForm = this._fb.group({
      email: ['', [<any>Validators.required]],
      password: ['', [<any>Validators.required]],
      confirmPassword: ['', [<any>Validators.required]],
      firstName: ['', [<any>Validators.required]],
      lastName: ['', [<any>Validators.required]],
      mobile: ['', [<any>Validators.required]],
      city: ['', [<any>Validators.required]],
      state: ['', [<any>Validators.required]],
      country: ['', [<any>Validators.required]],
      gender: ['Male', [<any>Validators.required]],
      profession: ['', [<any>Validators.required]]
    });


  }

  save(model: UserProfile, isValid: boolean) {
    this.submitted = true; // set form submit to true
    if (isValid) {

      this.data.createUser(model).then((success: any) => {
        {
          console.log("Success " + success.message);

          // this.data.isLoggedIn = true;
          // this.router.navigate([""]);
        }
      }).catch((error: any) => {
        console.log("Error " + error.message);

      });


    }
  }

}
